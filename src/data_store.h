#ifndef DATA_STORE_H
#define DATA_STORE_H

typedef struct {
    int hi_score;
} DataStore;

void load_data_store();
int get_hi_score();
void set_hi_score(int s);

#endif /* ifndef DATA_STORE_H */
