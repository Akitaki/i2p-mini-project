#ifndef GAME_H
#define GAME_H

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <allegro5/allegro.h>

#define LOG_ENABLED

typedef void (*func_ptr)(void);
typedef void (*func_ptr_keyboard)(int keycode);
typedef void (*func_ptr_mouse)(int btn, int x, int y, int dz);

typedef struct {
    char* name;
    func_ptr initialize;
    func_ptr update;
    func_ptr draw;
    func_ptr destroy;
    func_ptr_keyboard on_key_down;
    func_ptr_keyboard on_key_up;
    func_ptr_mouse on_mouse_down;
    func_ptr_mouse on_mouse_move;
    func_ptr_mouse on_mouse_up;
    func_ptr_mouse on_mouse_scroll;
} Scene;

// Frame rate (frame per second)
extern const int FPS;
// Display (screen) width.
extern const int SCREEN_W;
// Display (screen) height.
extern const int SCREEN_H;
// At most 4 audios can be played at a time.
extern const int RESERVE_SAMPLES;

// The active scene. Events will be triggered through function pointers.
extern Scene active_scene;
// Keyboard state, whether the key is down or not.
extern bool key_state[ALLEGRO_KEY_MAX];
// Mouse state, whether the key is down or not.
// 1 is for left, 2 is for right, 3 is for middle.
extern bool* mouse_state;
extern int mouse_x, mouse_y;

void game_create(void);
void game_change_scene(Scene next_scene);
void game_abort(const char* format, ...);

void game_log(const char* format, ...);

extern void shared_init(void);
extern void shared_destroy(void);

#endif
