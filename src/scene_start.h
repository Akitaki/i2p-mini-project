#ifndef SCENE_START_H
#define SCENE_START_H

#include "game.h"

typedef struct {
    int value;
    int cd;
} HP;

typedef struct {
    HP hp;
    int remain_lives;

    // Stats
    int score;
    int kills;

    double start_time;
    double end_time;

    int bullet_count;
    int hit_count;
} PlayerData;

Scene scene_start_create(void);

#endif
