#include "button.h"
#include "utility.h"
#include "game.h"

Button new_button(int xi, int yi, int w, int h, ALLEGRO_BITMAP* img, ALLEGRO_BITMAP* img_p) {
    Button btn = {
        .xi = xi,
        .yi = yi,
        .w = w,
        .h = h,
        .img = img,
        .img_pressed = img_p
    };
    return btn;
}

void draw_button(Button* btn) {
    if (pnt_in_rect(mouse_x, mouse_y, btn->xi, btn->yi, btn->w, btn->h)) {
        al_draw_bitmap(btn->img_pressed, btn->xi, btn->yi, 0);
    } else {
        al_draw_bitmap(btn->img, btn->xi, btn->yi, 0);
    }
}

bool clicked(Button* btn, int e_btn) {
    if (!e_btn)
        return false;

    bool in_rect = pnt_in_rect(mouse_x, mouse_y, btn->xi, btn->yi, btn->w, btn->h);
    if (in_rect)
        return true;
    return false;
}
